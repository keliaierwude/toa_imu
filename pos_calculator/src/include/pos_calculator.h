#include <ros/ros.h>
#include <zbar.h>
#include <iostream>
#include <iomanip>
#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/PointStamped.h>
#include <visualization_msgs/Marker.h>
#include <sensor_msgs/Imu.h>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include <ceres/ceres.h>

#include <camera_info_manager/camera_info_manager.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <std_msgs/Header.h>
#include <std_msgs/Float32.h>

#include <cmath>

#include "imu_utility.h"
#include "imu_factor.h"
#include "pose_local_parameterization.h"
#include "integration_base.h"

using namespace cv;
using namespace Eigen;


class Calculator
{
    public:

    Vector3d AP1;
    Vector3d AP2;
    Vector3d AP3;
    Vector3d distance;
    Vector3d f;
    Vector3d pi;
    Vector3d pi_old;
    Vector3d dpi;
    Vector3d pf;
    Matrix3d fd;
    Matrix3d fd1;
    Matrix4d imu2dis;
    double f_sum;
    double td;
    double IMU_dt;
    
    int WINDOW_SIZE;
    int count;

    IntegrationBase *pre_integrations[(WINDOW_SIZE + 1)];

    Vector3d Ps[(WINDOW_SIZE + 1)];
    Vector3d Vs[(WINDOW_SIZE + 1)];
    Matrix3d Rs[(WINDOW_SIZE + 1)];    
    Vector3d Bas[(WINDOW_SIZE + 1)];       
    Vector3d Bgs[(WINDOW_SIZE + 1)];
    double para_Pose[WINDOW_SIZE + 1][SIZE_POSE];
    double para_SpeedBias[WINDOW_SIZE + 1][SIZE_SPEEDBIAS];
    // Eigen::Matrix<double, 15, 15> jacobian, covariance;
    double latest_time{0};
    Vector3d latest_P{0,0,0}, delta_P{0,0,0}, latest_V{0,0,0}, latest_Ba{-0.0127947,0.64387,0.0560138}, latest_Bg{-0.00358294,0.0218833,0.0794294},
    latest_acc_0{0,0,0}, latest_gyr_0{0,0,0}, g{0,0,9.8}, imu_P{0,0,0};
    Matrix<double, 15, 15> jacobian, covariance;
    Quaterniond latest_Q{0,0,0,0};
    Quaterniond delta_q{0,0,0,0};
    Quaterniond result_delta_q{0,0,0,0};

    Calculator() : AP1(Vector3d::Zero()),AP2(Vector3d::Zero()),AP3(Vector3d::Zero()),distance(Vector3d::Zero()),f(Vector3d::Zero()),
                    pi(Vector3d::Zero()),pi_old(Vector3d::Zero()),dpi(Vector3d::Zero()),pf(Vector3d::Zero()),fd(Matrix3d::Zero()),
                    fd1(Matrix3d::Zero()),imu2dis(Matrix4d::Zero()),f_sum(1),td(0),IMU_dt(0),WINDOW_SIZE(10),count(0);
                    // jacobian{Eigen::Matrix<double, 15, 15>::Identity()}, covariance{Eigen::Matrix<double, 15, 15>::Zero()}
    {
    }
    ~Calculator(){}




    
    void Imu_predict(double t, Eigen::Vector3d linear_acceleration, Eigen::Vector3d angular_velocity);

    void optimization();

    void imu_initialization();

    void Visualization(const Eigen::Vector3d &pi);

    void slideWindow();

    void vector2double();

    void double2vector();

};

void distance_callback(const geometry_msgs::PointStamped::ConstPtr& dis_msg);

void imu_callback(const sensor_msgs::ImuConstPtr& imu_msg);

void ba_callback(const geometry_msgs::PointStamped::ConstPtr &ba_msg);
void bg_callback(const geometry_msgs::PointStamped::ConstPtr &bg_msg);

void Trilateration(Eigen::Vector3d dis_now_msg);

void process();
