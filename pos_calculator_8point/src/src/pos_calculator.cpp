#include <stdio.h>
#include <queue>
#include <map>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <std_msgs/Header.h>
#include <std_msgs/Float32.h>
#include <condition_variable>
#include <vector>
#include "gflags/gflags.h"
#include "glog/logging.h"


#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>

#include "/home/kerry/vins_ap/pos_calculator/src/include/pos_calculator.h"
#include "/home/kerry/vins_ap/pos_calculator/src/include/imu_utility.h"




int t;
int dis_count = 0;//push dis
int Q_flag = 0;//对其标志
int dis_flag;//判断dis是否错误
int isdis_flag ;//是否有图像传进
int imu_flag ; //判断是否完成一次预积分，进行初始化
int imu_first = 0;//判断是否为第一次接收imu消息，记录时间，方便imu和dis时间同步（仿真用处较大，实际可忽略）
int imu_buf_flag = 0;
int imu_contin,imu_count = 0;
double imu_t ,imu_firstt ,dis_t, dis_tnow = 0, dis_told = 0;
int align_flag = 0;//imu是否和dis对齐
int align_state = 0;//有dis消息  第一次尝试与imu对其
int dis_receive_flag = 0;//用于输出第一次dis时间
int bias_flag = 0;
int bias_pop_flag = 0;
int complete_flag = 0;
queue<sensor_msgs::ImuConstPtr> imu_buf;
queue<sensor_msgs::PointCloud> dis_buf;
queue<geometry_msgs::PointStamped> bg_buf;
queue<geometry_msgs::PointStamped> ba_buf;

std::mutex m_buf;

sensor_msgs::PointCloud distance_receive;
geometry_msgs::PointStamped bg_receive;
geometry_msgs::PointStamped ba_receive;
nav_msgs::Path path;
std_msgs::Header header;
// geometry_msgs::PointStamped distance_stamp;
ros::Publisher marker_pub;
ros::Publisher point;
ros::Publisher pub_path;

Calculator calculator;


int main(int argc, char **argv) {

    ros::init(argc,argv,"pos_generator");
    ros::NodeHandle nh1;
    cout<<"waiting for distance and imu......"<<endl;
    isdis_flag = 0;
    imu_flag = 0;
    

    ros::Subscriber bg_sub = nh1.subscribe("/bg",10, &bg_callback);
    ros::Subscriber ba_sub = nh1.subscribe("/ba",10, &ba_callback);
    ros::Subscriber distance_sub = nh1.subscribe("/distance", 10, &distance_callback);
    ros::Subscriber imu_sub = nh1.subscribe("/imu0", 2000, imu_callback, ros::TransportHints().tcpNoDelay());


    // message_filters::Subscriber<geometry_msgs::Point> distance_sub(nh1,"/distance",1);
    // message_filters::Subscriber<sensor_msgs::Imu> imu_sub(nh1,"/imu0", 2000, ros::TransportHints().tcpNoDelay());
    // TimeSynchronizer<geometry_msgs::Point,sensor_msgs::Imu>sync<image_sub,info_sub,10);
    // sync.registerCallback(boost::bind(&callback,_1_2));

    point = nh1.advertise<geometry_msgs::Point>("/dis_point",100);
    marker_pub = nh1.advertise<visualization_msgs::Marker>("dis_pose_marker", 100);
    pub_path = nh1.advertise<nav_msgs::Path>("path", 100);
    
    // std::thread measurement_process{process};
    ros::spin();
    return 0;

}




void Calculator::Imu_predict(double t, Eigen::Vector3d linear_acceleration, Eigen::Vector3d angular_velocity)
{
    // 中值积分,计算预计分delta_p
    IMU_dt = t - latest_time;
    //cout<<"IMU_dt:"<<IMU_dt<<endl;
    latest_time = t;
    Eigen::Vector3d un_acc_0 = delta_q * (latest_acc_0 - latest_Ba) - g;
    Eigen::Vector3d un_gyr = 0.5 * (latest_gyr_0 + angular_velocity) - latest_Bg;
    result_delta_q = delta_q * Imu_Utility::deltaQ(un_gyr * IMU_dt);
    Eigen::Vector3d un_acc_1 = result_delta_q * (linear_acceleration - latest_Ba) - g;
    Eigen::Vector3d un_acc = 0.5 * (un_acc_0 + un_acc_1);
    // cout<<"un_acc:"<<un_acc(0)<<","<<un_acc(1)<<","<<un_acc(2)<<","<<endl;
    // if(align_flag == 1)
    // {
    //     Vector4d delta_P_temp;
    //     delta_P_temp<< delta_P + IMU_dt * latest_V + 0.5 * IMU_dt * IMU_dt * un_acc,1;
    //     delta_P = ((delta_P_temp.transpose() * imu2dis).transpose()).block(0,0,3,1);
    // }
    // else
    // {
        delta_P = delta_P + IMU_dt * latest_V + 0.5 * IMU_dt * IMU_dt * un_acc;
    // }
    //cout<<"delta_P:"<<delta_P(0)<<","<<delta_P(1)<<","<<delta_P(2)<<endl;
    //imu_P = latest_P + delta_P;
    //cout<< "X:"<<latest_P.x()<<"Y:"<<latest_P.y()<<"Z:"<<latest_P.z()<<endl;
    //latest_V = latest_V + IMU_dt * un_acc;


    //compute jacobian
    // Vector3d w_x = 0.5 * (latest_gyr_0 + angular_velocity) - latest_Bg;
    // Vector3d a_0_x = un_acc_0 - latest_Ba;
    // Vector3d a_1_x = un_acc_1 - latest_Ba;
    // Matrix3d R_w_x, R_a_0_x, R_a_1_x;

    // R_w_x<<0, -w_x(2), w_x(1),
    // w_x(2), 0, -w_x(0),
    // -w_x(1), w_x(0), 0;
    // R_a_0_x<<0, -a_0_x(2), a_0_x(1),
    // a_0_x(2), 0, -a_0_x(0),
    // -a_0_x(1), a_0_x(0), 0;
    // R_a_1_x<<0, -a_1_x(2), a_1_x(1),
    // a_1_x(2), 0, -a_1_x(0),
    // -a_1_x(1), a_1_x(0), 0;

    // MatrixXd F = MatrixXd::Zero(15, 15);
    // F.block<3, 3>(0, 0) = Matrix3d::Identity();
    // F.block<3, 3>(0, 3) = -0.25 * delta_q.toRotationMatrix() * R_a_0_x * IMU_dt * IMU_dt + 
    //                     -0.25 * result_delta_q.toRotationMatrix() * R_a_1_x * (Matrix3d::Identity() - R_w_x * IMU_dt) * IMU_dt * IMU_dt;
    // F.block<3, 3>(0, 6) = MatrixXd::Identity(3,3) * IMU_dt;
    // F.block<3, 3>(0, 9) = -0.25 * (delta_q.toRotationMatrix() + result_delta_q.toRotationMatrix()) * IMU_dt * IMU_dt;
    // F.block<3, 3>(0, 12) = -0.25 * result_delta_q.toRotationMatrix() * R_a_1_x * IMU_dt * IMU_dt * -IMU_dt;
    // F.block<3, 3>(3, 3) = Matrix3d::Identity() - R_w_x * IMU_dt;
    // F.block<3, 3>(3, 12) = -1.0 * MatrixXd::Identity(3,3) * IMU_dt;
    // F.block<3, 3>(6, 3) = -0.5 * delta_q.toRotationMatrix() * R_a_0_x * IMU_dt + 
    //                     -0.5 * result_delta_q.toRotationMatrix() * R_a_1_x * (Matrix3d::Identity() - R_w_x * IMU_dt) * IMU_dt;
    // F.block<3, 3>(6, 6) = Matrix3d::Identity();
    // F.block<3, 3>(6, 9) = -0.5 * (delta_q.toRotationMatrix() + result_delta_q.toRotationMatrix()) * IMU_dt;
    // F.block<3, 3>(6, 12) = -0.5 * result_delta_q.toRotationMatrix() * R_a_1_x * IMU_dt * -IMU_dt;
    // F.block<3, 3>(9, 9) = Matrix3d::Identity();
    // F.block<3, 3>(12, 12) = Matrix3d::Identity();
    // //cout<<"A"<<endl<<A<<endl;

    // MatrixXd V = MatrixXd::Zero(15,18);
    // V.block<3, 3>(0, 0) =  0.25 * delta_q.toRotationMatrix() * IMU_dt * IMU_dt;
    // V.block<3, 3>(0, 3) =  0.25 * -result_delta_q.toRotationMatrix() * R_a_1_x  * IMU_dt * IMU_dt * 0.5 * IMU_dt;
    // V.block<3, 3>(0, 6) =  0.25 * result_delta_q.toRotationMatrix() * IMU_dt * IMU_dt;
    // V.block<3, 3>(0, 9) =  V.block<3, 3>(0, 3);
    // V.block<3, 3>(3, 3) =  0.5 * MatrixXd::Identity(3,3) * IMU_dt;
    // V.block<3, 3>(3, 9) =  0.5 * MatrixXd::Identity(3,3) * IMU_dt;
    // V.block<3, 3>(6, 0) =  0.5 * delta_q.toRotationMatrix() * IMU_dt;
    // V.block<3, 3>(6, 3) =  0.5 * -result_delta_q.toRotationMatrix() * R_a_1_x  * IMU_dt * 0.5 * IMU_dt;
    // V.block<3, 3>(6, 6) =  0.5 * result_delta_q.toRotationMatrix() * IMU_dt;
    // V.block<3, 3>(6, 9) =  V.block<3, 3>(6, 3);
    // V.block<3, 3>(9, 12) = MatrixXd::Identity(3,3) * IMU_dt;
    // V.block<3, 3>(12, 15) = MatrixXd::Identity(3,3) * IMU_dt;

    // jacobian = F * jacobian;
    // covariance = F * covariance * F.transpose() + V * noise * V.transpose();    
    
    latest_acc_0 = linear_acceleration;
    latest_gyr_0 = angular_velocity;
    delta_q = result_delta_q;
}


// std::vector<std::pair<std::vector<sensor_msgs::ImuConstPtr>, geometry_msgs::PointStamped::ConstPtr>>
// getMeasurements()
// {
//     std::vector<std::pair<std::vector<sensor_msgs::ImuConstPtr>, geometry_msgs::PointStamped::ConstPtr>> measurements;
//     geometry_msgs::PointStamped dis_now_msg;
//     geometry_msgs::PointStamped::ConstPtr dis_msg_temp;
//     while (true)
//     {
//         if (!(imu_buf.empty() || dis_buf.empty()))
//         {
//             flag = 1;
//         }

//         if(!dis_buf.empty())//有dis信息就pop出
//         {
//             dis_msg_temp = dis_buf.front();
//             dis_now_msg = *dis_msg_temp;
//             dis_buf.pop();
//             calculator.delta_P << 0,0,0;

//         }
//             std::cout<<"find dis and pop it"<<endl;
//         queue<sensor_msgs::ImuConstPtr> IMUs;
//         while (imu_buf.front()->header.stamp.toSec() < dis_buf.front()->header.stamp.toSec() + calculator.td)//判断两帧中间的imu，泵出所有
//         {
//             // IMUs.emplace_back(imu_buf.front());
//             IMUs.push(imu_buf.front());
//             imu_buf.pop();
//         }
//         //IMUs.emplace_back(imu_buf.front());
//         if (IMUs.empty())
//             ROS_WARN("no imu between two image");

//         measurements.emplace_back(IMUs, dis_msg_temp);
//     }
// }
void Calculator::imu_initialization()
{
    delta_P << 0,0,0;  
    //latest_acc_0 << 0,0,0;
    //latest_gyr_0 << 0,0,0;
    latest_V << 0,0,0;
    // delta_q.x() = 0;
    // delta_q.y() = 0;    
    // delta_q.z() = 0;
    // delta_q.w() = 0;
}

void process()
{
    // 将一帧信息放在measurements里
    //std::vector<std::pair<std::vector<sensor_msgs::ImuConstPtr>, geometry_msgs::PointStamped>> measurements;

    Vector3d old_point;
    MatrixXd dis_delta_point(15,4);
    MatrixXd imu_delta_point(15,4);   
    Vector3d q_cost;
    Vector3d P_cost;
    Matrix3d Jacobi_P;
    double align_cost;
    // int flag = 0;
    Vector3d Point;
    Matrix4d imu2dis_temp;

    // std::cout<< "processing"<<endl;

    
    // std::vector<std::pair<std::vector<sensor_msgs::ImuConstPtr>, geometry_msgs::PointStamped::ConstPtr>> measurements;
    // std::unique_lock<std::mutex> lk(m_buf);
    // con.wait(lk, [&]
    //          {
    //     return (measurements = getMeasurements()).size() != 0;
    //          });
    // lk.unlock();
    // geometry_msgs::PointStamped dis_now_msg;
    // queue<PointStamped::ConstPtr> dis_msg_temp;

    if (!(imu_buf.empty() || dis_buf.empty()))
    {
        
        if(imu_buf_flag == 0)
        {
            while(imu_buf.front()->header.stamp.toSec() < dis_buf.front().header.stamp.toSec()+calculator.td)
            {
                imu_buf.pop();
                imu_buf_flag = 1;        

            }
        }
        
        if(!dis_buf.empty())//有dis信息就pop出
        {
            double dis_1 = dis_buf.front().points[0].x;
            double dis_2 = dis_buf.front().points[1].x;
            double dis_3 = dis_buf.front().points[2].x;
            double dis_4 = dis_buf.front().points[3].x;
            double dis_5 = dis_buf.front().points[4].x;
            double dis_6 = dis_buf.front().points[5].x;
            double dis_7 = dis_buf.front().points[6].x;
            double dis_8 = dis_buf.front().points[7].x;
            double dis_now_msg[8] = {dis_1, dis_2, dis_3, dis_4, dis_5, dis_6, dis_7, dis_8};       
            // dis_now_msg = *dis_msg_temp;            
            header.stamp = dis_buf.front().header.stamp;
            header.frame_id = "world";  
            dis_buf.pop();
            calculator.imu_initialization();
            Trilateration(dis_now_msg);//dis计算位置
            bias_pop_flag = 0;

        }

        queue<sensor_msgs::ImuConstPtr> IMUs;
        while (imu_buf.front()->header.stamp.toSec() < dis_buf.front().header.stamp.toSec()+calculator.td)//判断两帧中间的imu，泵出所有
        {
            //std::cout<<"find imu and pop it"<<endl;
            // IMUs.emplace_back(imu_buf.front());
            if(bias_pop_flag == 0)
            {
                while(imu_buf.front()->header.stamp.toSec() > bg_buf.front().header.stamp.toSec())
                {
                    bg_buf.pop();
                    ba_buf.pop();
                }
            }
            bias_pop_flag = 1;
            calculator.latest_Ba(0) = ba_buf.front().point.x;
            calculator.latest_Ba(1) = ba_buf.front().point.y;
            calculator.latest_Ba(2) = ba_buf.front().point.z;            
            calculator.latest_Bg(0) = bg_buf.front().point.x;
            calculator.latest_Bg(1) = bg_buf.front().point.y;
            calculator.latest_Bg(2) = bg_buf.front().point.z;  
            IMUs.push(imu_buf.front());
            imu_buf.pop();
        }
        //IMUs.emplace_back(imu_buf.front());
        if (IMUs.empty())
            ROS_WARN("no imu between two image");


        if (align_flag == 1)//最小二乘联合估计位置
        {
            Vector3d dis_cost, imu_cost;
            //double cost = 1;

            //Ps = calculator.latest_P;//赋初值
            // while(fabs(cost) > 0.005)
            // {
            //     dis_cost = Ps - calculator.pi;
            //     imu_cost = Ps - calculator.imu_P;
            //     cout<<"imu_P:"<<calculator.imu_P(0)<<","<<calculator.imu_P(1)<<","<<calculator.imu_P(2)<<endl;
            //     cout<<"pi:"<<calculator.pi(0)<<","<<calculator.pi(1)<<","<<calculator.pi(2)<<endl;

            //     P_cost(0) = std::sqrt(dis_cost(0) * dis_cost(0) + imu_cost(0) * imu_cost(0));
            //     P_cost(1) = std::sqrt(dis_cost(1) * dis_cost(1) + imu_cost(1) * imu_cost(1));
            //     P_cost(2) = std::sqrt(dis_cost(2) * dis_cost(2) + imu_cost(2) * imu_cost(2));
            //     cout<<"P_cost:"<<P_cost(0)<<","<<P_cost(1)<<","<<P_cost(2)<<endl;

            //     cost = P_cost(0) + P_cost(1) + P_cost(2);

            //     Jacobi_P(0,0) = (dis_cost(0) + imu_cost(0)) / P_cost(0);
            //     Jacobi_P(0,1) = 0;
            //     Jacobi_P(0,2) = 0;
            //     Jacobi_P(1,1) = (dis_cost(1) + imu_cost(1)) / P_cost(1);
            //     Jacobi_P(1,1) = 0;
            //     Jacobi_P(1,2) = 0;        
            //     Jacobi_P(2,2) = (dis_cost(2) + imu_cost(2)) / P_cost(2); 
            //     Jacobi_P(2,1) = 0;
            //     Jacobi_P(2,2) = 0;  
            //     //cout<<"test1"<<endl;
            //     Ps(0) = Ps(0) - P_cost(0) / Jacobi_P(0,0);//最终值
            //     Ps(1) = Ps(1) - P_cost(1) / Jacobi_P(1,1);
            //     Ps(2) = Ps(2) - P_cost(2) / Jacobi_P(2,2);
                
            //     cout<<"Ps:"<<Ps(0)<<","<<Ps(1)<<","<<Ps(2)<<endl;
            //     //cout<<"test2"<<endl;
            // }                
            Point = 0.5 * (calculator.pi + calculator.delta_P + calculator.latest_P);
            if(fabs(Point(0))>100)
            {
              //calculator.latest_P = calculator.latest_P + calculator.delta_P;
               ROS_WARN("dis msg is wrong");
            } 
            else
            {
                calculator.latest_P = Point;
                complete_flag = 1;
                // cout<< "final pose:"<<Ps(0)<<","<<Ps(1)<<","<<Ps(2)<<endl;
                // calculator.slideWindow();
                // calculator.vector2double();
                // if (calculator.count = calculator.WINDOW_SIZE)
                // {
                //     calculator.optimization();
                //     calculator.double2vector();
                       calculator.Visualization(calculator.pi, header);//加入时间标签
                // }

            }
              

        }
        //calculation.optimization();

        while(!IMUs.empty())//imu不为空时一直计算预计分
        {
            imu_t = IMUs.front()->header.stamp.toSec();
            double dx = IMUs.front()->linear_acceleration.x;
            double dy = IMUs.front()->linear_acceleration.y;
            double dz = IMUs.front()->linear_acceleration.z;
            double rx = IMUs.front()->angular_velocity.x;
            double ry = IMUs.front()->angular_velocity.y;
            double rz = IMUs.front()->angular_velocity.z;
            Vector3d acc(dx, dy, dz);
            Vector3d gyr(rx, ry, rz);
            calculator.Imu_predict(imu_t,acc,gyr);//此处可以更新每一帧imu各项值，方便计算雅可比矩阵
            //cout<< "compute imu"<<endl;
            IMUs.pop();
        }

        if(align_flag == 0)//对齐
        {
            if(align_state == 0)
            {
                old_point = calculator.pi;            
                imu_delta_point.block(0,0,1,3) = calculator.delta_P.transpose();
                imu_delta_point.block(0,3,1,1) << 1;
                align_state = 1;
            }
            else if(align_state == 1)
            {
                if (Q_flag<15)
                {
                    dis_delta_point.block(Q_flag,0,1,3) = (calculator.pi - old_point).transpose();
                    dis_delta_point.block(Q_flag,3,1,1) << 1;
                    if(Q_flag<14)
                    {
                        imu_delta_point.block(Q_flag+1,0,1,3) = calculator.delta_P.transpose();
                        imu_delta_point.block(Q_flag+1,3,1,1) << 1;
                    }
                    old_point = calculator.pi;
                    //cout<<Q_flag<<endl;
                    Q_flag++;
                }
                else
                {

                    // imu2dis_temp = dis_delta_point * imu_delta_point.inverse(); 
                    imu2dis_temp = (imu_delta_point.transpose() * imu_delta_point).inverse() * imu_delta_point.transpose() * dis_delta_point; //最小二乘解旋转                   
                    
                    align_state = 2; 
                }
            }
            else
            {
                //cout<<"test1"<<endl;
                Vector4d imu_P_temp;
                imu_P_temp << calculator.delta_P,1;
                q_cost = (calculator.pi-old_point) - ((imu_P_temp.transpose() * imu2dis_temp).transpose()).block(0,0,3,1);//imu_delta_point.block(0,2,3,1);
                align_cost = std::sqrt(q_cost.x() * q_cost.x() + q_cost.y() * q_cost.y() + q_cost.z() * q_cost.z());

                if (fabs(align_cost) < 100000000)
                {
                    calculator.imu2dis = imu2dis_temp;
                    align_flag =1;
                    cout<< calculator.imu2dis(0,0)<<","<<calculator.imu2dis(0,1)<<","<<calculator.imu2dis(0,2)<<endl;
                    cout<< calculator.imu2dis(1,0)<<","<<calculator.imu2dis(1,1)<<","<<calculator.imu2dis(1,2)<<endl;
                    cout<< calculator.imu2dis(2,0)<<","<<calculator.imu2dis(2,1)<<","<<calculator.imu2dis(2,2)<<endl;
                    ROS_WARN("Initialization finish!");
                    // std::cout<<"the q is :"<<calculator.imu2dis<<endl;
                }
                else
                {
                    //cout<<"test2"<<endl;
                    align_state = 1;
                    Q_flag = 0;
                    imu_delta_point.block(0,0,1,3) = calculator.delta_P.transpose();
                    imu_delta_point.block(0,3,1,1) << 1;
                    ROS_WARN("Re-Initialized");
                }
            }
        }
    }       
}

void bg_callback(const geometry_msgs::PointStamped::ConstPtr &bg_msg)
{
    bg_receive = *bg_msg;
    //cout<<"receive bg msg"<<endl;
    //cout<<"bg_x:"<<bg_msg->point.x<<endl;
    //cout<<"bg_time"<<bg_msg->header.stamp.toSec()<<endl;
    if(bg_receive.point.x != 0)
    {
        m_buf.lock();
        bg_buf.push(bg_receive);
        m_buf.unlock();
        bias_flag = 1;
        //ROS_INFO("receive bias data, process begin");
    }

}

void ba_callback(const geometry_msgs::PointStamped::ConstPtr &ba_msg)
{
    //cout<<"receive ba msg"<<endl;
    if(bias_flag == 1)
    {
        ba_receive = *ba_msg;
        m_buf.lock();
        ba_buf.push(ba_receive);
        m_buf.unlock();        
    }

}

void distance_callback(const sensor_msgs::PointCloud::ConstPtr &dis_msg)
{
    //dis_tnow = ros::Time::now().toSec();
    //cout<<"receive dis msg"<<endl;
    if(bias_flag == 1)
    {
        distance_receive = *dis_msg;

        m_buf.lock();
        dis_buf.push(distance_receive);
        m_buf.unlock();
        dis_count++;
        //cout<<"here is dis msg"<<endl;
        // con.notify_one();    
        if (dis_receive_flag == 0)
        {
            cout << "first dis time" << distance_receive.header.stamp.toSec()<<endl;
            cout <<"real delta_dis time " <<  ros::Time::now().toSec() - distance_receive.header.stamp.toSec()<<endl;
            dis_receive_flag = 1;
        }
        else
        {
            //cout<< "not the first dis" << endl;
        }
        
        if(dis_count > 2)
        {
            process();
        }        
    }

 

    // dis_t = dis_tnow - dis_told;

    // cout<<"solving position"<<endl;
   
    // Trilateration(distance_receive);   
    
    // if(imu_contin == 0)
    // {
    //     calculator.Visualization(calculator.pi);
    // }


    // imu_contin = 0;
    // isdis_flag = 1;
    // dis_told = dis_tnow;

    return;
}

void imu_callback(const sensor_msgs::ImuConstPtr &imu_msg)
{
    // imu_t = imu_msg->header.stamp.toSec();
    // double dx = imu_msg->linear_acceleration.x;
    // double dy = imu_msg->linear_acceleration.y;
    // double dz = imu_msg->linear_acceleration.z;
    // double rx = imu_msg->angular_velocity.x;
    // double ry = imu_msg->angular_velocity.y;
    // double rz = imu_msg->angular_velocity.z;
    // Vector3d acc(dx, dy, dz);
    // Vector3d gyr(rx, ry, rz);
    //cout<<"receive imu msg"<<endl;
    if(bias_flag == 1)
    {
        if(dis_receive_flag == 1)
        {
            m_buf.lock();
            imu_buf.push(imu_msg);//存imu数据
            m_buf.unlock();

            if(imu_flag == 0)
            {
                calculator.latest_time = imu_msg->header.stamp.toSec();
                imu_flag = 1;
            }
            //cout<<"here is imu msg"<<endl;
            // con.notify_one();    
            if (imu_first == 0)
            {
                imu_first = 1;
                calculator.td = imu_msg->header.stamp.toSec()- ros::Time::now().toSec();
                cout<<"first imu time: "<< imu_msg->header.stamp.toSec()<<endl;
                cout<<"real imu time " <<  ros::Time::now().toSec()<<endl;
                cout<<"td"<<calculator.td<<endl;
            }
        }        
    }



    // cout<<"imu time"<< imu_t - imu_firstt <<endl;
    // cout<<"dis time"<< dis_t <<endl;


    // if ((imu_flag == 0) || (isdis_flag == 0) || (dis_flag == 0))
    // {
    //     calculator.latest_time = imu_t;
    //     calculator.latest_acc_0 = acc;
    //     calculator.latest_gyr_0 = gyr;
    //     calculator.latest_P = calculator.pi;
    //     imu_flag = 1;
    //     imu_firstt = imu_t;
    //     cout<<"first imu"<<endl;

    // }
    // else if ((imu_t - imu_firstt) < dis_t )
    // {
    //     cout<<"imu processing"<<endl;
    //     calculator.ImP_cost(0)u_predict(imu_t, acc, gyr);
    //     calculator.dpP_cost(0)i = calculator.latest_P - calculator.pi;
    //     // imu_count+P_cost(0)+;
    // }P_cost(0)
    // elseP_cost(0)
    // {P_cost(0)
    //     cout<<"initiaP_cost(0)l imu for next prediction"<<endl;
    //     imu_flag = 0;
    //     // cout<<"imu_count:"<<imu_count<<endl;
    //     imu_count = 0;
    //     imu_firstt = imu_t;
    // }
    

    return;
}

struct toa_cost_function_define {

    template <typename T>
    bool operator()(const T* const x, T* residual) const{
    
    // calculator.AP_temp(8,3);
    calculator.AP_temp << 0, 2, -1,
                        0, 2, 3,
                        0, -2, -1,
                        0, -2, 3,
                        9, 2, -1,
                        9, 2, 3,
                        9, -2, -1,
                        9, -2, 3;

    double a1[8], a2[8], a3[8];

    
    for(int i = 0; i < calculator.AP_NUM; i++)
    {
        a1[i] = calculator.AP_temp(i,0);
        a2[i] = calculator.AP_temp(i,1);
        a3[i] = calculator.AP_temp(i,2);
        residual[i] = (T(a1[i]) - x[0]) * (T(a1[i]) - x[0]) +
                    (T(a2[i]) - x[1]) * (T(a2[i]) - x[1]) +
                    (T(a3[i]) - x[2]) * (T(a3[i]) - x[2]) - T(calculator.distance(i) * T(calculator.distance(i)));
    }
    return true;
    }
};

void Trilateration(double dis_now_msg[8])
{
    using ceres::AutoDiffCostFunction;
    using ceres::CostFunction;
    using ceres::Problem;
    using ceres::Solver;
    using ceres::Solve;
    // MatrixXd AP(3,AP_NUM);
    // MatrixXd AP_temp(AP_NUM,3);
    // VectorXd distance(AP_NUM);
    // Vector3d delta_AP2P[AP_NUM];
    // calculator.AP1 << 2, 2, 1;
    // calculator.AP2 << 3, -4, -1;
    // calculator.AP3 << 6, 1, 2;  //ap_position
    //AP = AP_temp.trampose();
    for(int i = 0; i < calculator.AP_NUM; i++)
    {
        calculator.distance(i) = dis_now_msg[i];
    }


    // cout<<"test"<<endl;
    Problem problem;
    Solver::Options options;
    options.max_num_iterations = 100;
    options.linear_solver_type = ceres::DENSE_QR;
    //options.minimizer_progress_to_stdout = true;
    Solver::Summary summary;
    // double x[3] = {0,0,0.1};
    CostFunction* toa_costfunction = new AutoDiffCostFunction<cost_function_define, 8, 3>(
    	new toa_cost_function_define);
    problem.AddResidualBlock(costfunction,NULL,calculator.pi);
    Solve(options,&problem,&summary);
    // calculator.pi(0) = x[0];
    // calculator.pi(1) = x[1];
    // calculator.pi(2) = x[2];
    



    // calculator.distance(0) = dis_now_msg(0);
    // calculator.distance(1) = dis_now_msg(1);
    // calculator.distance(2) = dis_now_msg(2);


    // if((calculator.pi(0) == 0) && (calculator.pi(1) == 0) &&(calculator.pi(0) == 0))
    // {
    //     calculator.pi << 0,0,0;
    //     calculator.pi_old << 0,0,0.1;
    //     dis_flag = 0;
    //     cout<<"first time"<<endl;
    //     // cout<<"pi:"<<calculator.pi(0)<<","<<calculator.pi(1)<<","<<calculator.pi(2)<<","<<endl;
    // }    
    // else if((fabs(calculator.f_sum)>10000000000) && (dis_flag == 1))
    // {
    //     cout<<"pi is wrong, use the last position"<<endl;
    //     calculator.pi = calculator.pi_old + calculator.dpi;
    //     // calculator.pi = calculator.pi_old+calculator.latest_P;
    // }
    // // else if(fabs(calculator.pi(2) - calculator.pi_old(2)) > 1)
    // // {
    // //     imu_contin = 1;
    // // }
    // // else if (complete_flag == 1)
    // // {
    // //     cout<<"exact predict the next position"<<endl;
    // //     calculator.pi = calculator.pi + calculator.delta_P;
    // //     dis_flag = 1;
    // // }
    // else
    // {
    //     cout<<"predict the next position"<<endl;
    //     calculator.pi_old = calculator.pi;//保存此次位置作为原始数据pi_old

    //     calculator.pi = calculator.pi + calculator.dpi;
    //     // calculator.pi = calculator.pi + calculator.latest_P;
    //     dis_flag = 1;
    // }


    // calculator.f_sum = 1;
    // t=0;
    // //while(t<10)
    // while(fabs(calculator.f_sum) > 0.005)
    // {
    //     // cout<<"..."<<endl;
    //     if(t % 30 == 0)
    //     {
    //         //cout<<"f_sum:"<<calculator.f_sum<<"..."<<endl;
    //     }
        
    //     // calculator.f(0) = std::sqrt((calculator.pi(0)-calculator.AP1(0)) * (calculator.pi(0)-calculator.AP1(0)) + 
    //     //                         (calculator.pi(1)-calculator.AP1(1)) * (calculator.pi(1)-calculator.AP1(1)) + 
    //     //                         (calculator.pi(2)-calculator.AP1(2)) * (calculator.pi(2)-calculator.AP1(2)))-calculator.distance(0);
    //     // calculator.f(1) = std::sqrt((calculator.pi(0)-calculator.AP2(0)) * (calculator.pi(0)-calculator.AP2(0)) + 
    //     //                         (calculator.pi(1)-calculator.AP2(1)) * (calculator.pi(1)-calculator.AP2(1)) + 
    //     //                         (calculator.pi(2)-calculator.AP2(2)) * (calculator.pi(2)-calculator.AP2(2)))-calculator.distance(1);
    //     // calculator.f(2) = std::sqrt((calculator.pi(0)-calculator.AP3(0)) * (calculator.pi(0)-calculator.AP3(0)) + 
    //     //                         (calculator.pi(1)-calculator.AP3(1)) * (calculator.pi(1)-calculator.AP3(1)) + 
    //     //                         (calculator.pi(2)-calculator.AP3(2)) * (calculator.pi(2)-calculator.AP3(2)))-calculator.distance(2);
    //     for(i = 0; i < AP_NUM; i++)
    //     {
            
    //         distance(i) = dis_now_msg(i);
    //         delta_AP2P[i] = pi - AP.block(0,i,3,1);
    //         f(i) = sqrt(delta_AP2P[i](0) * delta_AP2P[i](0) + delta_AP2P[i](1) * delta_AP2P[i](1) + delta_AP2P[i](2) * delta_AP2P[i](2)) - distance(i);
    //     }



    //     calculator.f_sum = std::sqrt(calculator.f(0) * calculator.f(0) +  calculator.f(1) * calculator.f(1) +calculator.f(2) * calculator.f(2));
        //cout<<"f_sum:"<<calculator.f_sum<<endl;

        // calculator.fd(0,0) = (1/calculator.f(0)) * (calculator.pi(0) - calculator.AP1(0));
        // calculator.fd(0,1) = (1/calculator.f(0)) * (calculator.pi(1) - calculator.AP1(1));
        // calculator.fd(0,2) = (1/calculator.f(0)) * (calculator.pi(2) - calculator.AP1(2)); //df(0,0)
    

        // calculator.fd(1,0) = (1/calculator.f(1)) * (calculator.pi(0) - calculator.AP2(0)); //df(0,1)
        // calculator.fd(1,1) = (1/calculator.f(1)) * (calculator.pi(1) - calculator.AP2(1));
        // calculator.fd(1,2) = (1/calculator.f(1)) * (calculator.pi(2) - calculator.AP2(2));

        // calculator.fd(2,0) = (1/calculator.f(2)) * (calculator.pi(0) - calculator.AP3(0));
        // calculator.fd(2,1) = (1/calculator.f(2)) * (calculator.pi(1) - calculator.AP3(1));
        // calculator.fd(2,2) = (1/calculator.f(2)) * (calculator.pi(2) - calculator.AP3(2)); 

        // cout<<"fd:"<<calculator.fd(0,0)<<","<<calculator.fd(0,1)<<","<<calculator.fd(0,2)<<","<<calculator.fd(1,0)<<","<<calculator.fd(1,1)
        // <<","<<calculator.fd(1,2)<<","<<calculator.fd(2,0)<<","<<calculator.fd(2,1)<<","<<calculator.fd(2,2)<<endl;

        // calculator.fd1 = calculator.fd.inverse();
        // calculator.pf = calculator.fd1 * calculator.f;
        // //cout<<"pf:"<<calculator.pf(0)<<endl;

        // calculator.pi = calculator.pi - calculator.pf;


        // t++;
        // if(fabs(calculator.f_sum)>10000000000)
        // {
        //     break;
        //     isdis_flag =0;
        //     calculator.pi = calculator.latest_P;
        // }
    //calculator.fd_sum = calculator.fd(0) + calculator.fd(1) + calculator.fd(2);
    // }

    


    //cout<<"f_sum:"<<calculator.f_sum<<endl;
    cout<<"dis_point: "<< calculator.pi(0)<<","<< calculator.pi(1)<<","<<calculator.pi(2)<<endl;


    // visualization_msgs::Marker marker;

    // marker.header.frame_id = "world";
    // marker.header.stamp = ros::Time::now();

    // // 设置该标记的命名空间和ID，ID应该是独一无二的
    // // 具有相同命名空间和ID的标记将会覆盖前一个
    // marker.ns = "basic_shapes";
    // marker.id = 0;
 
    // // 设置标记类型，初始值为立方体。在立方体、球体、箭头和 圆柱体之间循环
    // marker.type = visualization_msgs::Marker::CUBE;

    // // 设置标记行为：ADD（添 加），DELETE（删 除）
    // marker.action = visualization_msgs::Marker::ADD;

    // //设置标记位姿。 
    // marker.pose.position.x = calculator.pi(0);
    // marker.pose.position.y = calculator.pi(1);
    // marker.pose.position.z = calculator.pi(2);

    // //统一设置旋转，大小，颜色
    // marker.pose.orientation.x = 0.0;
    // marker.pose.orientation.y = 0.0;
    // marker.pose.orientation.z = 0.0;
    // marker.pose.orientation.w = 1.0;

    // marker.scale.x = 0.17;
    // marker.scale.y = 0.17;
    // marker.scale.z = 0.01;


    // marker.color.r = 0.0f;
    // marker.color.g = 0.0f;
    // marker.color.b = 1.0f;
    // marker.color.a = 1.0f;

    // // calculator.dpi = calculator.pi - calculator.pi_old;
    // marker_pub.publish(marker);
    isdis_flag =0;

}

void Calculator::Visualization(const Eigen::Vector3d &pi, const std_msgs::Header &header)
{
    visualization_msgs::Marker marker;

    marker.header.stamp = ros::Time::now();
    marker.header.frame_id = "world";
    

    // 设置该标记的命名空间和ID，ID应该是独一无二的
    // 具有相同命名空间和ID的标记将会覆盖前一个
    marker.ns = "basic_shapes";
    marker.id = 0;
 
    // 设置标记类型，初始值为立方体。在立方体、球体、箭头和 圆柱体之间循环
    marker.type = visualization_msgs::Marker::CUBE;

    // 设置标记行为：ADD（添 加），DELETE（删 除）
    marker.action = visualization_msgs::Marker::ADD;

    //设置标记位姿。 
    marker.pose.position.x = pi.x();
    marker.pose.position.y = pi.y();
    marker.pose.position.z = pi.z();

    //统一设置旋转，大小，颜色
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    marker.scale.x = 0.17;
    marker.scale.y = 0.17;
    marker.scale.z = 0.01;


    marker.color.r = 0.0f;
    marker.color.g = 0.0f;
    marker.color.b = 1.0f;
    marker.color.a = 1.0f;

    // geometry_msgs::PoseStamped pose_stamped;
    // pose_stamped.header = header;
    // pose_stamped.header.frame_id = "world";
    // pose_stamped.pose = odometry.pose.pose;
    // path.header = header;
    // path.header.frame_id = "world";
    // path.poses.push_back(pose_stamped);
    // pub_path.publish(path);

    // calculator.dpi = calculator.pi - calculator.pi_old;
    marker_pub.publish(marker);
}

// void Calculator::optimization()
// {
//     ceres::Problem problem;
//     ceres::LossFunction *loss_function;
//     //loss_function = new ceres::HuberLoss(1.0);
//     loss_function = new ceres::CauchyLoss(1.0);

//     // 添加优化变量，位置及误差   
//     for (int i = 0; i < WINDOW_SIZE + 1; i++)
//     {
//         //ceres::LocalParameterization *local_parameterization = new PoseLocalParameterization();
//         //problem.AddParameterBlock(para_Pose[i], 9, local_parameterization);
//         problem.AddParameterBlock(para_Pose[i], 3);
//         // problem.AddParameterBlock(para_SpeedBias[i], 9);
//     }
//     // 添加RT矩阵参数，位置
//     // ceres::LocalParameterization *local_parameterization = new PoseLocalParameterization();
//     // problem.AddParameterBlock(dis_Pose, 3, local_parameterization);

//     //添加imu的residual
//     // for (int i = 0; i < WINDOW_SIZE; i++)
//     // {
//     //     int j = i + 1;
//     //     IMUFactor* imu_factor = new IMUFactor(pre_integrations[j]);
//     //     problem.AddResidualBlock(imu_factor, NULL, para_Pose[i], para_SpeedBias[i], para_Pose[j], para_SpeedBias[j]);
//     // }
//     //添加toa的residual
//     DISFactor* dis_factor = new DISFactor(dis_i);//传入当前dis参数
//     problem.AddResidualBlock(dis_factor, loss_function, para_Pose[i]);

//     ceres::Solver::Options options;

//     options.linear_solver_type = ceres::DENSE_SCHUR;
//     //options.num_threads = 2;
//     options.trust_region_strategy_type = ceres::DOGLEG;
//     options.max_num_iterations = 20;

//     ceres::Solver::Summary summary;
//     ceres::Solve(options, &problem, &summary);   
// }

// void Calculator::slideWindow()
// {
//     if (count < WINDOW_SIZE + 1)
//     {
//         Ps[count] = latest_P;
//         Vs[count] = latest_V;
//         //Rs[count] = ;//给当前旋转赋值    
//         Bas[count] = latest_Ba;       
//         Bgs[count] = latest_Bg;
//         count ++;
//     }
//     if (count = WINDOW_SIZE +1)
//     {
//         for(i = 0 , i < WINDOW_SIZE +1 , i++)
//         {
//             Rs[i].swap(Rs[i + 1]);

//             // std::swap(pre_integrations[i], pre_integrations[i + 1]);

//             // linear_acceleration_buf[i].swap(linear_acceleration_buf[i + 1]);
//             // angular_velocity_buf[i].swap(angular_velocity_buf[i + 1]);

//             // Headers[i] = Headers[i + 1];
//             Ps[i].swap(Ps[i + 1]);
//             Vs[i].swap(Vs[i + 1]);
//             Bas[i].swap(Bas[i + 1]);
//             Bgs[i].swap(Bgs[i + 1]);
//         }
//         Ps[count] = latest_P;
//         Vs[count] = latest_V;
//         //Rs[count] = ;//给当前旋转赋值    
//         Bas[count] = latest_Ba;       
//         Bgs[count] = latest_Bg;
//     }
// }

// void Calculator::vector2double()
// {
//     for (int i = 0; i < WINDOW_SIZE + 1; i++)
//     {
//         para_Pose[i][0] = Ps[i].x();
//         para_Pose[i][1] = Ps[i].y();
//         para_Pose[i][2] = Ps[i].z();
//         // Quaterniond q{Rs[i]};
//         // para_Pose[i][3] = q.x();
//         // para_Pose[i][4] = q.y();
//         // para_Pose[i][5] = q.z();
//         // para_Pose[i][6] = q.w();

//         para_SpeedBias[i][0] = Vs[i].x();
//         para_SpeedBias[i][1] = Vs[i].y();
//         para_SpeedBias[i][2] = Vs[i].z();

//         para_SpeedBias[i][3] = Bas[i].x();
//         para_SpeedBias[i][4] = Bas[i].y();
//         para_SpeedBias[i][5] = Bas[i].z();

//         para_SpeedBias[i][6] = Bgs[i].x();
//         para_SpeedBias[i][7] = Bgs[i].y();
//         para_SpeedBias[i][8] = Bgs[i].z();
//     }
//     //RT关系矩阵
//     // for (int i = 0; i < NUM_OF_CAM; i++)
//     // {
//     //     para_Ex_Pose[i][0] = tic[i].x();
//     //     para_Ex_Pose[i][1] = tic[i].y();
//     //     para_Ex_Pose[i][2] = tic[i].z();
//     //     Quaterniond q{ric[i]};
//     //     para_Ex_Pose[i][3] = q.x();
//     //     para_Ex_Pose[i][4] = q.y();
//     //     para_Ex_Pose[i][5] = q.z();
//     //     para_Ex_Pose[i][6] = q.w();
//     // }

//     // VectorXd dep = f_manager.getDepthVector();
//     // for (int i = 0; i < f_manager.getFeatureCount(); i++)
//     //     para_Feature[i][0] = dep(i);
//     // if (ESTIMATE_TD)
//     //     para_Td[0][0] = td;
// }

// void Calculator::double2vector()
// {
//     for(i = 0, i < WINDOW_SIZE +1 , i++)
//     {
//         Ps[i] = Vector3d(para_Pose[i][0] , para_Pose[i][1] , para_Pose[i][2]);   
//     }

// }