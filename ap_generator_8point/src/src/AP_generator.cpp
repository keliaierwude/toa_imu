# include "data_generator.h"
# include "geometry_msgs/PointStamped.h"
# include "sensor_msgs/PointCloud.h"


using namespace std;
using namespace Eigen;

nav_msgs::Odometry odometry;

ros::Publisher marker_pub;
ros::Publisher distance_pub;    

sensor_msgs::PointCloud pos;
// geometry_msgs::Point AP_visual;
// geometry_msgs::Point p;

APGenerator AP_generator;

int main(int argc, char **argv) {

    ros::init(argc,argv,"AP_generator");
    ros::NodeHandle nh;
    cout<<"waiting for odometry"<<endl;

    ros::Subscriber odometry_sub = nh.subscribe("/vins_estimator/odometry", 10, &odometry_callback);
    distance_pub = nh.advertise<sensor_msgs::PointCloud>("/distance",10);
    marker_pub = nh.advertise<visualization_msgs::Marker>("visualization_marker", 1);
    
    ros::spin();
    return 0;

}



void odometry_callback(const nav_msgs::Odometry::ConstPtr& msg)
{

    
    odometry = *msg;
    MatrixXd AP(AP_generator.AP_NUM,3);

    // AP_generator.AP1 << 0, 1, -1;
    // AP_generator.AP2 << 0, 1, 5;
    // AP_generator.AP3 << 0, -1, -1;
    // AP_generator.AP4 << 0, -1, 5;
    // AP_generator.AP5 << 15, 1, -1;
    // AP_generator.AP6 << 15, 1, 5;
    // AP_generator.AP7 << 15, -1, -1;
    // AP_generator.AP8 << 15, -1, 5;
    AP << 0, 2, -1,
        0, 2, 3,
        0, -2, -1,
        0, -2, 3,
        9, 2, -1,
        9, 2, 3,
        9, -2, -1,
        9, -2, 3;
 
    // AP_generator.AP1 = AP_generator.APpoints.block(0,0,1,3);
    // AP_generator.AP2 = AP_generator.APpoints.block(0,1,1,3);
    // AP_generator.AP3 = AP_generator.APpoints.block(0,2,1,3);

    // cout<< "AP1 location: "<< AP_generator.AP1(0)<<","<<AP_generator.AP1(1)<<","<<AP_generator.AP1(2)<<endl;
    // cout<< "AP2 location: "<< AP_generator.AP2(0)<<","<<AP_generator.AP2(1)<<","<<AP_generator.AP2(2)<<endl;
    // cout<< "AP3 location: "<< AP_generator.AP3(0)<<","<<AP_generator.AP3(1)<<","<<AP_generator.AP3(2)<<endl;

    AP_generator.pose(0) = odometry.pose.pose.position.x;
    AP_generator.pose(1) = odometry.pose.pose.position.y;
    AP_generator.pose(2) = odometry.pose.pose.position.z;

    pos.header.stamp = ros::Time::now();
    pos.points.resize(AP_generator.AP_NUM);
    for (int i = 0; i < AP_generator.AP_NUM; i++)
    {
        // cout<<"test1"<<endl;
        Vector3d vector_temp;
        vector_temp = AP.block(i,0,1,3).transpose() - AP_generator.pose;
        pos.points[i].x = sqrt(vector_temp(0) * vector_temp(0) + vector_temp(1) * vector_temp(1) + vector_temp(2) * vector_temp(2));
        // cout<<"test2"<<endl;
        pos.points[i].y = pos.points[i].z = 0;

    }


    // AP_generator.distance(0) = sqrtf((AP_generator.AP1(0)-AP_generator.pose(0))*(AP_generator.AP1(0)-AP_generator.pose(0)) + 
    //                                 (AP_generator.AP1(1)-AP_generator.pose(1))*(AP_generator.AP1(1)-AP_generator.pose(1)) +
    //                                 (AP_generator.AP1(2)-AP_generator.pose(2))*(AP_generator.AP1(2)-AP_generator.pose(2)));

    // AP_generator.distance(1) = sqrtf((AP_generator.AP2(0)-AP_generator.pose(0))*(AP_generator.AP2(0)-AP_generator.pose(0)) + 
    //                                 (AP_generator.AP2(1)-AP_generator.pose(1))*(AP_generator.AP2(1)-AP_generator.pose(1)) +
    //                                 (AP_generator.AP2(2)-AP_generator.pose(2))*(AP_generator.AP2(2)-AP_generator.pose(2)));

    // AP_generator.distance(2) = sqrtf((AP_generator.AP3(0)-AP_generator.pose(0))*(AP_generator.AP3(0)-AP_generator.pose(0)) + 
    //                                 (AP_generator.AP3(1)-AP_generator.pose(1))*(AP_generator.AP3(1)-AP_generator.pose(1)) +
    //                                 (AP_generator.AP3(2)-AP_generator.pose(2))*(AP_generator.AP3(2)-AP_generator.pose(2)));

    // cout<<" distance1: "<<AP_generator.distance(0)<<endl;
    // cout<<" distance2: "<<AP_generator.distance(1)<<endl;
    // cout<<" distance3: "<<AP_generator.distance(2)<<endl;

    cout<<" distance1: "<<pos.points[0].x<<endl;
    cout<<" distance2: "<<pos.points[1].x<<endl;
    cout<<" distance3: "<<pos.points[2].x<<endl;
    // pos.dis_1 = AP_generator.distance(0);
    // pos.dis_2 = AP_generator.distance(1);
    // pos.dis_3 = AP_generator.distance(2);
    // pos.dis_4 = AP_generator.distance(3);
    // pos.dis_5 = AP_generator.distance(4);
    // pos.dis_6 = AP_generator.distance(5);    
    // pos.dis_7 = AP_generator.distance(6);
    // pos.dis_8 = AP_generator.distance(7);


    distance_pub.publish(pos);

    visualization_msgs::Marker marker1,marker2,marker3,marker4,marker5,marker6,marker7,marker8,line;

    // 设置帧 ID和时间戳
    marker1.header.frame_id = marker2.header.frame_id = marker3.header.frame_id = marker4.header.frame_id = 
    marker5.header.frame_id = marker6.header.frame_id = marker7.header.frame_id = marker8.header.frame_id = line.header.frame_id ="world";
    marker1.header.stamp = marker2.header.stamp = marker3.header.stamp = marker4.header.stamp = marker5.header.stamp =
    marker6.header.stamp = marker7.header.stamp = marker8.header.stamp =line.header.stamp = ros::Time::now();

    // 设置该标记的命名空间和ID，ID应该是独一无二的
    // 具有相同命名空间和ID的标记将会覆盖前一个
    marker1.ns = marker2.ns = marker3.ns = marker4.ns = marker5.ns = marker6.ns = marker7.ns = marker8.ns ="basic_shapes";
    marker1.id = 0;
    marker2.id = 1;
    marker3.id = 2;
    marker4.id = 3;
    marker5.id = 4;
    marker6.id = 5;
    marker7.id = 6;
    marker8.id = 7;
    // line.id = 8;


    // 设置标记类型，初始值为立方体。在立方体、球体、箭头和 圆柱体之间循环
    marker1.type = visualization_msgs::Marker::CUBE;
    marker2.type = visualization_msgs::Marker::CUBE;
    marker3.type = visualization_msgs::Marker::CUBE;
    marker4.type = visualization_msgs::Marker::CUBE;
    marker5.type = visualization_msgs::Marker::CUBE;
    marker6.type = visualization_msgs::Marker::CUBE;
    marker7.type = visualization_msgs::Marker::CUBE;
    marker8.type = visualization_msgs::Marker::CUBE;
    // line.type = visualization_msgs::Marker::LINE_LIST;


    // 设置标记行为：ADD（添 加），DELETE（删 除）
    marker1.action = visualization_msgs::Marker::ADD;
    marker2.action = visualization_msgs::Marker::ADD;
    marker3.action = visualization_msgs::Marker::ADD;
    marker4.action = visualization_msgs::Marker::ADD;
    marker5.action = visualization_msgs::Marker::ADD;
    marker6.action = visualization_msgs::Marker::ADD;
    marker7.action = visualization_msgs::Marker::ADD;
    marker8.action = visualization_msgs::Marker::ADD;
    //设置标记位姿。 
    // marker1.pose.position.x = AP_generator.AP1(0);
    // marker1.pose.position.y = AP_generator.AP1(1);
    // marker1.pose.position.z = AP_generator.AP1(2);

    // marker2.pose.position.x = AP_generator.AP2(0);
    // marker2.pose.position.y = AP_generator.AP2(1);
    // marker2.pose.position.z = AP_generator.AP2(2);

    // marker3.pose.position.x = AP_generator.AP3(0);
    // marker3.pose.position.y = AP_generator.AP3(1);
    // marker3.pose.position.z = AP_generator.AP3(2);
    marker1.pose.position.x = AP(0,0);
    marker1.pose.position.y = AP(0,1);
    marker1.pose.position.z = AP(0,2);

    marker2.pose.position.x = AP(1,0);
    marker2.pose.position.y = AP(1,1);
    marker2.pose.position.z = AP(1,2);

    marker3.pose.position.x = AP(2,0);
    marker3.pose.position.y = AP(2,1);
    marker3.pose.position.z = AP(2,2);

    marker4.pose.position.x = AP(3,0);
    marker4.pose.position.y = AP(3,1);
    marker4.pose.position.z = AP(3,2);

    marker5.pose.position.x = AP(4,0);
    marker5.pose.position.y = AP(4,1);
    marker5.pose.position.z = AP(4,2);

    marker6.pose.position.x = AP(5,0);
    marker6.pose.position.y = AP(5,1);
    marker6.pose.position.z = AP(5,2);

    marker7.pose.position.x = AP(6,0);
    marker7.pose.position.y = AP(6,1);
    marker7.pose.position.z = AP(6,2);

    marker8.pose.position.x = AP(7,0);
    marker8.pose.position.y = AP(7,1);
    marker8.pose.position.z = AP(7,2);


    //统一设置旋转，大小，颜色
    marker1.pose.orientation.x = marker2.pose.orientation.x = marker3.pose.orientation.x = marker4.pose.orientation.x = 
    marker5.pose.orientation.x = marker6.pose.orientation.x = marker7.pose.orientation.x = marker8.pose.orientation.x = 0.0;

    marker1.pose.orientation.y = marker2.pose.orientation.y = marker3.pose.orientation.y = marker4.pose.orientation.y = 
    marker5.pose.orientation.y = marker6.pose.orientation.y = marker7.pose.orientation.y = marker8.pose.orientation.y = 0.0;

    marker1.pose.orientation.z = marker2.pose.orientation.z = marker3.pose.orientation.z = marker4.pose.orientation.z = 
    marker5.pose.orientation.z = marker6.pose.orientation.z = marker7.pose.orientation.z = marker8.pose.orientation.z = 0.0;

    marker1.pose.orientation.w = marker2.pose.orientation.w = marker3.pose.orientation.w = marker4.pose.orientation.w = 
    marker5.pose.orientation.w = marker6.pose.orientation.w = marker7.pose.orientation.w = marker8.pose.orientation.w =1.0;

    marker1.scale.x = marker2.scale.x = marker3.scale.x = marker4.scale.x = marker5.scale.x = marker6.scale.x = 
    marker7.scale.x = marker8.scale.x = 0.17;
    marker1.scale.y = marker2.scale.y = marker3.scale.y = marker4.scale.y = marker5.scale.y = marker6.scale.y =
    marker7.scale.y = marker8.scale.y = 0.17;
    marker1.scale.z = marker2.scale.z = marker3.scale.z = marker4.scale.z = marker5.scale.z = marker6.scale.z =
    marker7.scale.z = marker8.scale.z = 0.01;
    // line.scale.x = 0.01;

    marker1.color.r = marker2.color.r = marker3.color.r = marker4.color.r = marker5.color.r = marker6.color.r =
    marker7.color.r = marker8.color.r = 0.0f;
    marker1.color.g = marker2.color.g = marker3.color.g = marker4.color.g = marker5.color.g = marker6.color.g =
    marker7.color.g = marker8.color.g = 1.0f;
    marker1.color.b = marker2.color.b = marker3.color.b = marker4.color.b = marker5.color.b = marker6.color.b =
    marker7.color.b = marker8.color.b = 0.0f;
    marker1.color.a = marker2.color.a = marker3.color.a = marker4.color.a = marker5.color.a = marker6.color.a =
    marker7.color.a = marker8.color.a = 1.0f;
    // line.color.r = 1.0f;
    // line.color.a = 1.0f;
    
    //画线

    
    // p.x = AP_generator.pose(0);
    // p.y = AP_generator.pose(1);
    // p.z = AP_generator.pose(2);
    // AP.x = AP_generator.AP1(0);
    // AP.y = AP_generator.AP1(1);
    // AP.z = AP_generator.AP1(2);
    
    // line.points.push_back(p);
    // line.points.push_back(AP);

    // AP.x = AP_generator.AP2(0);
    // AP.y = AP_generator.AP2(1);
    // AP.z = AP_generator.AP2(2);
    // line.points.push_back(p);
    // line.points.push_back(AP);

    // AP.x = AP_generator.AP3(0);
    // AP.y = AP_generator.AP3(1);
    // AP.z = AP_generator.AP3(2);
    // line.points.push_back(p);
    // line.points.push_back(AP);
        


    marker_pub.publish(marker1);
    marker_pub.publish(marker2);
    marker_pub.publish(marker3);
    marker_pub.publish(marker4);
    marker_pub.publish(marker5);
    marker_pub.publish(marker6);
    marker_pub.publish(marker7);
    marker_pub.publish(marker8);
    // marker_pub.publish(line);
}



    
