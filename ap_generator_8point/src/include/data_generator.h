#ifndef ZBAR_OPENCV_H
#define ZBAR_OPENCV_H

#include <ros/ros.h>
#include <zbar.h>
#include <iostream>
#include <iomanip>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>

#include <camera_info_manager/camera_info_manager.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cmath>


using namespace cv;
using namespace Eigen;


class APGenerator
{
    public:

    // Vector3d AP1;
    // Vector3d AP2;
    // Vector3d AP3;
    // Vector3d AP4;
    // Vector3d AP5;
    // Vector3d AP6;
    // Vector3d AP7;
    // Vector3d AP8;
    Vector3d pose;
    // VectorXd distance;
    int AP_NUM;
    
    APGenerator() : 
    // AP1(Vector3d::Zero()),AP2(Vector3d::Zero()),AP3(Vector3d::Zero()),
    // AP4(Vector3d:Zero()),
    // AP5(Vector3d::Zero()),AP6(Vector3d::Zero()),AP7(Vector3d::Zero()),AP8(Vector3d:Zero()),
    pose(Vector3d::Zero())
    // ,distance(VectorXd::Zero())
    ,AP_NUM(8)
    {
    }
    ~APGenerator(){}
    


};

void odometry_callback(const nav_msgs::Odometry::ConstPtr& msg);



#endif