# TOA_IMU
-----
# src
## pos_calculator
### receive the message

*receive all the message we need and push all of them into each queque.*

- void distance_callback

receive the TOA message, the distance to each AP. And run the process fuction as long as receive the distance message
	
- void imu_callback 

receive the imu message，the accelerated speed in real time.
	
- *UWB_callback*

receive the UWB message, the rotation in real time. (not finished yet)

- *ba_callback & bg_callback*

recieve the bias data from Vins, only use for simulation without UWB.
### process the image

- void process

Process all the message in this fuction, including TOA, pre-integration，alignment, loose coupling， tight coupling and visualization.

If each queue is not empty, then pop message from each queue by judging the time stamp, and finally get all the message we need at time K.

1. **void Trilateration:**  
This fuction receive three distance message and compute the current location by using the Newton iteration method. Then we can get a predict position
TOA_Pose at time K. 

2. **void Imu_predict:**  
This fuction receive current imu message and compute the delta pose by using Median Integral. Then we can get a predict delta position delta_P at time K.

3. **alignment:**     
The rotation message from IMU and UWB should be the same. Thus, by using this relationship can compute the bias of IMU and the rotation matrix between TOA and IMU.
Judging the alignment by the *align_flag*, if the flag is 1 means the alignment finished and go on next step.

4. **loose coupling:** 
Predict the position by using delta_P and TOA_Pose.

5. **tight coupling:**  
Compute the jacobi matrix and covariance matrix of IMU, TOA and margin. Then use ceres to compute the final pose.
6. **visualization:**  
Input the time stamp and positon.

-----
# include
- pos_calculator.h    
The library for main fuction, define the class calculator.    
- imu_utility.h  
The library for computing Quaternion.  
- imu_factor.h & integration_base.h  
The library for defining the Imu cost fuction for ceres and compute the imu residual.(not finished yet)  
- pose_local_parameterization.h  
The library for defining the TOA cost fution for ceres.(not finished yet)  

-----
# how to run 8point estimator

* only estimate the position by using the 8 point * 
- catkin_make every file named 8point
- roscore
- rorun rviz
- rosrun ap_generator ap_genrator
- rosrun pos_calculator pos_calcualtor
- follow the guidence of Vins-Fusion and run an example

# how to run toa_imu

* This program can conpute the position by using tight coupling. *
- catkin_make the file named toa_imu
- roscore 
- rosrun rviz
- rosrun ap_generator ap_generator
- rosrun toa_imu toa_imu
- follow the guidence of Vins-Fusion and run an example